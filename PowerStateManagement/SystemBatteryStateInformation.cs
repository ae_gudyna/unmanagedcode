﻿using System.Runtime.InteropServices;

namespace PowerStateManagement
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SystemBatteryStateInformation
    {
        public byte AcOnLine;
        public byte BatteryPresent;
        public byte Charging;
        public byte Discharging;
        public byte Spare1;
        public byte Tag;
        public int MaxCapacity;
        public int RemainingCapacity;
        public int Rate;
        public int EstimatedTime;
        public int DefaultAlert1;
        public int DefaultAlert2;
    }
}
