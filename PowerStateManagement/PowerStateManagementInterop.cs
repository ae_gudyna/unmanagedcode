﻿using System;
using System.Runtime.InteropServices;

namespace PowerStateManagement
{
    internal class PowerStateManagementInterop
    {
        internal enum PowerInformationLevel
        {
            SystemBatteryState = 5,
            SystemReserveHiberFile = 10,
            SystemPowerInformation = 12,
            LastWakeTime = 14,
            LastSleepTime = 15
        }

        [DllImport("powrprof.dll", EntryPoint = "CallNtPowerInformation")]
        public static extern NtStatus CallNtPowerInformation(
          PowerInformationLevel InformationLevel,
          IntPtr InputBuffer,
          int InputBufferLength,
          IntPtr OutputBuffer,
          int OutputBufferLength
        );

        [DllImport("powrprof.dll", EntryPoint = "SetSuspendState")]
        public static extern int SetSuspendState(
          bool bHibernate,
          bool bForce,
          bool bWakeupEventsDisabled
        );
    }
}
