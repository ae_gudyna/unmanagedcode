﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PowerStateManagement
{
    [ComVisible(true)]
    [Guid("1E24A499-C6FE-4FD5-9520-D3E354E28D77")]
    [ClassInterface(ClassInterfaceType.None)]
    public class PowerStateManager : IPowerStateManager
    {
        public SystemPowerInformation CallSystemPowerInformation()
        {
            return CallInformation<SystemPowerInformation>(PowerStateManagementInterop.PowerInformationLevel.SystemPowerInformation);
        }

        public SystemBatteryStateInformation CallSystemBatteryStateInformation()
        {
            return CallInformation<SystemBatteryStateInformation>(PowerStateManagementInterop.PowerInformationLevel.SystemBatteryState);
        }

        public long CallLastSleepTimeInformation()
        {
            return CallInformation<long>(PowerStateManagementInterop.PowerInformationLevel.LastSleepTime);
        }

        public long CallLastWakeTimeInformation()
        {
            return CallInformation<long>(PowerStateManagementInterop.PowerInformationLevel.LastWakeTime);
        }

        public bool Suspend()
        {
            var state = PowerStateManagementInterop.SetSuspendState(true, false, false);
            if (state == 0) return false;
            return true;
        }
        public bool ReserveHiberFile(bool reserve)
        {
            var size = Marshal.SizeOf(typeof(byte));
            var byteReserve = (byte)(reserve ? 1 : 0);
            IntPtr inputBuffer = Marshal.AllocCoTaskMem(size);
            Marshal.WriteByte(inputBuffer, size, byteReserve);
            var state = PowerStateManagementInterop.CallNtPowerInformation(
                PowerStateManagementInterop.PowerInformationLevel.SystemReserveHiberFile,
                inputBuffer,
                size,
                IntPtr.Zero,
                0);
            Marshal.FreeCoTaskMem(inputBuffer);
            if (state != NtStatus.Success) return false;
            return true;
        }

        private T CallInformation<T>(PowerStateManagementInterop.PowerInformationLevel powerInformationLevel){
            T returnedValue = default(T);
            var size = Marshal.SizeOf(typeof(T));
            IntPtr outputBuffer = Marshal.AllocCoTaskMem(size);
            var state = PowerStateManagementInterop.CallNtPowerInformation(
                powerInformationLevel,
                IntPtr.Zero,
                0,
                outputBuffer,
                size);
            if (state == NtStatus.Success)
            {
                returnedValue = (T)Marshal.PtrToStructure(outputBuffer, typeof(T));
            }
            if (outputBuffer != IntPtr.Zero)
                Marshal.FreeCoTaskMem(outputBuffer);
             
            return returnedValue;
        }
    }
}
