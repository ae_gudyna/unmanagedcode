﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PowerStateManagement
{
    [ComVisible(true)]
    [Guid("7A35F7A8-8F32-4C73-80D1-98212DB2B6C1")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    interface IPowerStateManager
    {
        SystemPowerInformation CallSystemPowerInformation();
        SystemBatteryStateInformation CallSystemBatteryStateInformation();
        long CallLastSleepTimeInformation();
        long CallLastWakeTimeInformation();
        bool ReserveHiberFile(bool reserve);
        bool Suspend();
    }
}
