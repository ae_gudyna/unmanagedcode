using System;
using System.Runtime.InteropServices;
using Xunit;
using Xunit.Abstractions;

namespace PowerStateManagement.Test
{
    public class PowerStateManagerTest
    {
        private readonly ITestOutputHelper output;

        public PowerStateManagerTest(ITestOutputHelper output)
        {
            this.output = output;
        }
        [Fact]
        public void SystemPowerTest()
        {
            var manager = new PowerStateManager();
            var spi = manager.CallSystemPowerInformation();
            output.WriteLine(spi.TimeRemaining.ToString());
        }
        [Fact]
        public void SystemBatteryStateTest()
        {
            var manager = new PowerStateManager();
            var sbsi = manager.CallSystemBatteryStateInformation();
            output.WriteLine(sbsi.Charging.ToString());
        }

        [Fact]
        public void LastWakeTimeTest()
        {
            var manager = new PowerStateManager();
            var wti = manager.CallLastWakeTimeInformation();
            output.WriteLine(wti.ToString());
        }
        
        [Fact]
        public void LastSleepTimeTest()
        {
            var manager = new PowerStateManager();
            var wti = manager.CallLastSleepTimeInformation();
            output.WriteLine(wti.ToString());
        }

        [Fact]
        public void HiberTest()
        {
            var manager = new PowerStateManager();
            var success = manager.ReserveHiberFile(true);
                output.WriteLine(success.ToString());
        }


        [Fact]
        public void SetSuspendStateTest()
        {
            var manager = new PowerStateManager();
            var success = manager.Suspend();
            output.WriteLine(success.ToString());
        }
    }
}
